---
title: 图标表情选择器
index: false
icon: laptop-code
category:
    - 组件
---

## 图标表情选择器组件

```html
<SelectIcon 
    @isIconShow="iconVisible = false" 
    @getAvatar="getAvatar"
    v-show="iconVisible" 
    :isCustom="isCustom" 
    :customTitle="customTitle">
</SelectIcon>
```
页面展示预览需要绑定样式
否则icon的颜色选择将会失效
由于使用了阴影和移动，外围需要套一个容器防止元素溢出
```html
<div class="overflow-hidden">
    <a-avatar :src="avatarUrl" 
        style="height:64px;width: 64px;border-radius: 0;" 
        :style="{'filter': bgColor?`drop-shadow(#${bgColor} 80px 0)`:'',transform:bgColor?'translateX(-80px)':''}">
    </a-avatar>
</div>
```

```html
相对路径 vite\packages\selectIcon\page\index.vue
```

## 属性

| 属性    | 说明     | 类型   | 默认值 |
| :------ | :------- | :----- | :----- |
| isCustom   | 是否需要自定义区域     | Boolean |        |
| customTitle | 自定义提示文字 | string |        |
| bgColor | 如果用户选择了图标颜色，需要进行替换 | string |        |

## 事件

| 属性    | 说明         |
| :------ | :----------- |
| isIconShow | 关闭当前选择器窗口 需提前定义iconVisible来接收 |
| getAvatar | 传递表情或图标的地址 需提前定义avatarUrl来接收 |

```html
// 获取头像
getAvatar(avatar){
    if(avatar.indexOf('color=') >= 0){
        let color = avatar.substr(avatar.indexOf('color=') + 7 ,6)
        this.bgColor = color
    }else{
        this.bgColor = ''
    }
    this.avatarUrl = avatar
},
```
