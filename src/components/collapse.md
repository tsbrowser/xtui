---
title: 折叠面板
index: false
icon: laptop-code
category:
    - 组件
---

## 按钮组件

```html
<xt-collapse title="123" content="321"></xt-collapse>
```

## 属性

| 属性    | 说明     | 类型   | 默认值 |
| :------ | :------- | :----- | :----- |
| title   | 标题     | string |        |
| content | 内容区域 | string |        |

## 插槽

| 属性    | 说明         |
| :------ | :----------- |
| default | 替换文本内容 |
| title   | 替换文本标题 |
