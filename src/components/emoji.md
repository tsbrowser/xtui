---
title: Emoji组件
index: false
icon: laptop-code
category:
  - 组件
---

## Icon组件

emoji表情组件，
注意，此组件不需要加xt-前缀
推荐在此处浏览，选择emoji，然后以png形式保存到项目的public/emoji项目下
再使用此组件
```html
<emoji icon=""></emoji>
``` 

## API

| 属性    | 说明               | 类型     | 默认值|
|:------|:-----------------|:-------|:-|
| icon  | 表情的属性，和emoji文件同名 | string | - | 