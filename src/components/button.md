---
title: 按钮
index: false
icon: laptop-code
category:
    - 组件
---

## 按钮组件

```html
<xt-button>按钮</xt-button>
<!-- 可使用style="width: 100%"实现百分比宽度 -->
<xt-button style="width: 100%">按钮</xt-button>
```

## 属性

| 属性         | 说明                                                                      | 类型   | 默认值  |
| :----------- | :------------------------------------------------------------------------ | :----- | :------ |
| type         | 类型风格 default 是 xt-bg-2，theme 跟随系统主题， error warn link success | string | default |
| iconPosition | icon 的位置 prefix postfix                                                | string | prefix  |
| icon         | icon                                                                      | string |         |
| w            | 宽度                                                                      | string | 120     |
| h            | 高度                                                                      | string | 48      |
| h            | 高度                                                                      | string | 48      |

## 插槽

| 属性    | 说明         |
| :------ | :----------- |
| default | 存放按钮文字 |
