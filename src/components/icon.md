---
title: Icon图标组件
index: false
icon: laptop-code
category:
  - 组件
---

## Icon组件

图标组件，
注意，此组件不需要加xt-前缀
```html
<icon icon=""></icon>
``` 

## API
此图标需要在iconfont上先导入到项目中方可使用

`注意：如果需要图标支持变色，需要单独对iconfont图标进行去色处理。否则，不管你用样式怎么去调整，最终只能得到一个原色图标。`

`小心：请不要全部去色，只能选择你的图标进行去色。`

| 属性    | 说明            | 类型     | 默认值|
|:------|:--------------|:-------|:-|
| icon  | 图标，取出icons-前缀 | string | - | 