---
title: Icon图标组件
index: false
icon: laptop-code
category:
    - 组件
---

## Icon 组件

```html
<xt-icon icon=""></xt-icon>
```

## 属性

| 属性 | 说明                                                               | 类型   | 默认值  |
| :--- | :----------------------------------------------------------------- | :----- | :------ |
| icon | 图标，取出 icons-前缀                                              | string | -       |
| w    | 调整宽高                                                           | string | 48      |
| copy | 传入需要复制的内容 即可触发复制到剪切板                            | string |         |
| type | 类型风格 default 是 xt-bg-2，theme 跟随系统主题，可传入空 取消类型 | string | default |
| size | icon 大小                                                          | string | 26      |
