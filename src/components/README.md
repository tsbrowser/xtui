---
title: UI组件
index: false
icon: laptop-code
---

## 未补全的组件

所有的组件仅用于工作台二次开发，不可用于第三方应用开发。

文档上没有可以先去目录看看有没有相关组件
browser\vite\packages\table\ui 查看源码或者搜索组件相关使用方式
基于 antd 组件封装存放在 libs
基于项目业务的组件封装的存放在 component

## 目录

-   [组件总览](overview.md)

-   [通用]()
    -   [Button 按钮](button.md)
    -   [Icon 图标](icon.md)
    -   [Emoji Emoji 表情](emoji.md)
-   [反馈]()

    -   [Modal 对话框](modal.md)

-   [社交]()
