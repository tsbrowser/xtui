---
title: Icon图标组件
index: false
icon: laptop-code
category:
    - 组件
---

## 基础用法

```html
<xt-new-icon></xt-new-icon>
```

## 全属性展示

设计一个背景红色 大小 60 icon 黄色 大小 30 模板

```html
<xt-new-icon
    icon="majesticons:monitor-line"
    bgStyle="red"
    color="yellow"
    size="30"
    w="60"
/>
```

## 属性

| 属性    | 说明                                          | 类型   | 默认值 |
| :------ | :-------------------------------------------- | :----- | :----- |
| icon    | 图标名                                        | string | -      |
| w       | 调整外层宽高 在 bg 样式或类名 激活时有效 居中 | string | 48     |
| size    | icon 大小                                     | string | 26     |
| bgStyle | 样式写法 背景色                               | string |        |
| bgClass | 类名写法 背景色                               | string |        |
| color   | 图标颜色                                      | string | #000   |
