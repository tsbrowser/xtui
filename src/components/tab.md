## 横向 可以直接设置这个 tab 的宽度 会动态适配

```vue
<template>
    <xt-tab
        style="width: 260px; height: 120px"
        :list="tabList"
        v-model="selectTab"
    ></xt-tab>
</template>
```

## 纵向 通过 itemClass 或者 itemStyle 控制宽高

```vue
<template>
    <xt-tab
        :list="tabList"
        v-model="selectTab"
        mode="col"
        placement="right"
        itemClass="h-16 w-16"
    ></xt-tab>
</template>
```

## 属性

| 属性       | 说明                                                                                                                  | 类型   | 默认值                                   |
| :--------- | :-------------------------------------------------------------------------------------------------------------------- | :----- | :--------------------------------------- |
| placement  | 气泡框位置，可选 top left right bottom topLeft topRight bottomLeft bottomRight leftTop leftBottom rightTop rightBotto | string | top                                      |
| mode       | 对齐方式 可选 row col                                                                                                 | string | row                                      |
| boxClass   | 样式                                                                                                                  | string |                                          |
| boxStyle   | 样式                                                                                                                  | string |                                          |
| itemClass  | 样式                                                                                                                  | string |                                          |
| itemStyle  | 样式                                                                                                                  | string |                                          |
| modelValue | v-model 绑定数据 是选择的数据                                                                                         | string |                                          |
| active     | 选择的类名                                                                                                            | string | xt-active-btn                            |
| name       | 自定义数组显示字段名                                                                                                  | string | name                                     |
| value      | 自定义数组数据字段名                                                                                                  | string | value                                    |
| icon       | 自定义数组图标字段名                                                                                                  | string | icon                                     |
| tip        | 自定义数组提示字段名                                                                                                  | string | tip                                      |
| list       | tab 循环内容                                                                                                          | obj    | [{ name: "2 x 2", value: "card small" }] |
