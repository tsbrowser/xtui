---
title: tsbApi
index: false
icon: laptop-code
category:
  - 社交
---

### tsbApi是我们暴露到应用环境中的通用api，此套api甚至支持到了工作台内。
可以直接从window对象上取得。

通过这套api，你可以方便地与应用直接进行交互。包括window的一系列操作。


tsbApi.runtime.name