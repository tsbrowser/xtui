---
title: 应用机制
icon: lightbulb
---
我们基于wujie.js研发了第三方应用的开发机制。
注意：应用机制从10.x版本开始加入支持，10.x以下版本无法使用第三方应用。

推荐第三方应用的开发技术栈：
[Vue3](https://cn.vuejs.org/)+[pinia](https://pinia.vuejs.org/zh/)+[AntDesignVue4](https://www.antdv.com/docs/vue/introduce-cn)


## 核心优势

与传统的一些工具相比，天天工作台的应用有如下优势：

1.天然对副屏支持良好，零适配即可跟随工作台缩放，用户天然可获得良好的使用体验

2.多入口，支持桌面卡片、导航栏图标卡片、工具箱（待开发）、弹窗主应用、独立窗口应用（原轻应用，待开发）

3.自带社交属性。可联动元社区，通过定义对应社区版块ID，即可在应用的快捷菜单快速访问。甚至还支持代码级的唤起社交功能。
社交功能包括：元社区版块、社群聊天

4.提供工作台原生API，可以联动工作台的系统级功能

## 推荐项目

### 无界
- [wujie.js](https://github.com/Tencent/wujie)
`无界.js微前端框架`
### 知识库Todo、Markdown、脑图多合一应用
- [知识库](https://gitee.com/qiaoshengda/knowledge-base)
`这是一个用于收集、整理和编辑信息流的一站式仓库，帮助您在信息洪流中摘录有价值的内容，进行整理再输出，打造自己的知识体系。`
`此应用包含2个卡片（待办、写笔记）和一个弹窗主应用`
![image.png](https://jxxt-1257689580.cos.ap-chengdu.myqcloud.com/lWWg-MPVz-9_wA-hkNv)
![image.png](https://jxxt-1257689580.cos.ap-chengdu.myqcloud.com/op2P-J-2W-_nAS-PvHX)
![image.png](https://jxxt-1257689580.cos.ap-chengdu.myqcloud.com/jiSw-Rs30-p7od-cXDz)
### HiLab gitlab小助手
- [HiLab](https://gitee.com/tsbrowser/hi-lab)
`基于gitlabApi的快捷管理小组件`
`此应用包含1个卡片`
![image.png](https://jxxt-1257689580.cos.ap-chengdu.myqcloud.com/2Xhe-Yo53-fk6Z-deAo)