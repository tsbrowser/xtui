---
title: 业务组件
index: false
icon: laptop-code
category:
    - 业务
---

### 业务组件

业务组件仅可用于天天工作台二次开发，不可用于第三方应用开发