## 支线任务配置

### 支线任务添加位置

browser\vite\packages\table\apps\task\config\Branch.ts

### 开始任务时所跳转的方向配置

browser\vite\packages\table\apps\task\page\branch\guide.ts

### 在完成任务的函数中添加方法

browser\vite\packages\table\apps\task\page\branch\task.ts

```vue
<script>
import { completeTask } from "../apps/task/page/branch/task"; // 根据上面的路径自行调整

completeTask("支线任务 ID"); // 在完成任务的地方调用
</script>
```

## 主线任务

### 主线任务添加位置

browser\vite\packages\table\apps\task\page\branch\task.ts

### 主线任务开始的前置和结束的后置

browser\vite\packages\table\apps\task\page\lastGuide.ts
browser\vite\packages\table\apps\task\page\endGuide.ts

### 主线任务每个指引点配置项

browser\vite\packages\table\ui\components\Task\guide.ts

### 实际使用

```vue
<xt-task id="任务编号" no="步骤编号" @cb="实际触发的方法;">
被包裹的区域
</xt-task>
```
