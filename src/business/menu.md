## 使用

```vue
<template>
    <xt-menu :menus="menus">
        <div>右键菜单触发的区域</div>
    </xt-menu>
</template>

<script setup>
// icon可不传
const menus = [{ label: "更新", callBack: this.callBack, icon: "shizhi1" }];
</script>
```
