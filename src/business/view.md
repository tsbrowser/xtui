## 基本用法

```html
<xt-view></xt-vivw>
```

## 弹窗视图

```vue
<template>
    <xt-view type="popup" title="标题" v-model="viewVisible">
        <div style="width: 400px; height: 400px">默认内容</div>
    </xt-view>
    <xt-button @click="viewVisible = !viewVisible">viewVisible</xt-button>
</template>
<script>
export default {
    data() {
        return {
            viewVisible: true,
        };
    },
};
</script>
```

插槽

```html
<xt-view>
    <template #left> 占据整个左侧 </template>
    <template #header> 头部 </template>
    <div>默认内容</div>
</xt-view>
```

获取全屏状态

```html
<xt-view v-model:full="full"> </xt-view>
```

## 属性

| 属性     | 说明                                | 类型   | 默认值                 |
| :------- | :---------------------------------- | :----- | :--------------------- |
| type     | 视图的类型                          | string | default [popup]        |
| title    | 快速编写标题                        | string |                        |
| showFull | 关闭全屏按钮                        | obj    | true                   |
| slot     | 视图内容的插槽                      |        | default [left、header] |
| full     | 获取视图全屏状态                    | obj    | false                  |
| visible  | 通过 v-model 控制弹窗层的显示和关闭 | obj    | true                   |
| spacing  | 控制间距                            | number | 3                      |

## 插槽

| 属性    | 说明     |
| :------ | :------- |
| default | 主插槽   |
| header  | 头部插槽 |
| left    | 左侧插槽 |

## 事件

| 属性  | 说明                                                |
| :---- | :-------------------------------------------------- |
| close | 关闭窗口的回调                                      |
| esc   | 按 esc 触发关闭回调 前提是绑定了 visible 或者 close |
