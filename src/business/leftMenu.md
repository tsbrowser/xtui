## 完整用例

```vue
<template>
    <xt-left-menu :list="menuList" last="2" end="2">
        <!-- 插槽用法 -->
        <template #test>
            <setting-filled />
        </template>
    </xt-left-menu>

    <!-- 新建对话 -->
    <xt-view
        v-model="createChatVisible"
        type="popup"
        title="新建模板"
        :showFull="false"
    >
        <xt-button w="300"> new Caht View </xt-button>
    </xt-view>
</template>

<script>
import { SettingFilled } from "@ant-design/icons-vue";
export default {
    components: {
        SettingFilled,
    },
    data() {
        return {
            menuList: [
                // 携带子菜单
                {
                    icon: "message",
                    children: [
                        {
                            icon: "message",
                            name: "message",
                            callBack: () => {
                                console.log("子菜单点击事件触发 :>> ");
                            },
                        },
                        {
                            icon: "message",
                            name: "message",
                            callBack: () => {
                                console.log("子菜单点击事件触发 :>> ");
                            },
                        },
                    ],
                },
                // iconfont
                {
                    icon: "message",
                },
                // 图片
                {
                    img: "/icons/bg.png",
                },
                // 插槽
                {
                    slot: "test",
                    callBack: () => {
                        this.createChatVisible = true;
                    },
                },
                // 回调事件
                {
                    flag: true,
                    icon: "tianjia2",
                    callBack: () => {
                        this.createChatVisible = true;
                    },
                },
                {
                    flag: true,
                    icon: "setting",
                },
            ],
            createChatVisible: false,
        };
    },
};
</script>

<style lang="scss" scoped></style>
```

## 属性

| 属性  | 说明                                               | 类型   | 默认值     |
| :---- | :------------------------------------------------- | :----- | :--------- |
| last  | 头部菜单展示数量                                   | string | 1          |
| end   | 尾部菜单展示数量                                   | string | 1          |
| index | 默认选择的菜单 有待优化 通过下标选择 应该优化为 ID | string | true       |
| id    | 首次选中的 ID 如果没传 ID 内部会进行添加           | string | 默认第一个 |
| list  | 排好序的数组 用法看完整用例                        | array  |            |

## 插槽

| 属性    | 说明           |
| :------ | :------------- |
| default | 传递 antd 组件 |
