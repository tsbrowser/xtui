## 基本用法

```vue
<template>
    <xt-modal v-model="flag"></xt-modal>
</template>
<script setup>
import { ref } from "vue";
const flag = ref(true);
</script>
```

使用插槽

```vue
<template>
    <xt-modal v-model="flag">
        <template #headerLeft> 头部左侧 </template>
        <template #header> 头部中心 </template>
        <template #headerRight> 头部右侧 </template>
        <template> 主体区域 </template>
        <template #footer> 替换默认底部button </template>
    </xt-modal>
</template>

<script setup>
import { ref } from "vue";
const flag = ref(true);
</script>
```

## 属性

| 属性  | 说明         | 类型   | 默认值 |
| :---- | :----------- | :----- | :----- |
| title | 快速编写标题 | string |        |
| esc   | esc 关闭     |        | false  |

## 插槽

| 属性        | 说明           |
| :---------- | :------------- |
| default     | 主插槽         |
| headerLeft  | 头部左侧插槽   |
| header      | 头部中心区插槽 |
| headerRight | 头部右侧插槽   |
| footer      | 底部插槽       |

## 事件

| 属性  | 说明                                       |
| :---- | :----------------------------------------- |
| close | 关闭窗口的回调                             |
| esc   | 按 esc 触发关闭回调 前提是 esc 属性为 true |
| ok    | 默认点击按钮回调                           |
