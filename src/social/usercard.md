---
title: 用户卡片组件
index: false
category:
  - 组件
---

## userCard组件

userCard组件比较特殊。由于使用场景较多，采用的是预埋组件，然后通过编程式方法触发。
第一步，在你的触发组件上埋点
```html
<xxx @click="showCard(uid,userInfo)" ></xxx>
``` 
其中uid和userInfo是两个你已经在其他地方获取到的需要显示用户卡片的用户的信息，其中userInfo可选。
如果填写了userInfo，可以大大减少用户卡片弹出的时间。

第二步，在methods里写showCard方法
```javascript
import { appStore } from '../../store'

export default{

  methods: {
    ...mapActions(appStore, ['showUserCard']),
    showCard(uid,userInfo){
      this.showUserCard(uid,userInfo)
    }
  }
}
```
userInfo的数据格式：
```javascript

{
    uid:uid,
    nickname:data.user.nickname,
    avatar:data.user.avatar_128,
    signature:data.user.signature,
    certification:data.user.all_certification_entity_pc||[]
}
```
数据填写越多，载入效果越好。

## API

`注意：建议尽量提交userInfo对象，以加速弹出速度。`

| 属性       | 说明     | 类型     | 默认值 |
|:---------|:-------|:-------|:----|
| uid      | 用户的uid | number | 必填  | 
| userInfo | 用户信息   | object | 可选  | 