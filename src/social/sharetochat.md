---
title: 分享到聊天组件
index: false
category:
  - 组件
---

## ShareToChat组件

ShareToChat组件主要是封装了分享到聊天的功能。包括选择会话。


第一步.界面中引入分享组件
```javascript
import ShareToChat from '../../ui/chat/ShareToChat.vue'
```
```html
<ShareToChat :visible="shareVisible" @close="shareVisible=false"
             :content="shareContent" type="game" alias='游戏'></ShareToChat>
```

第二步.构建分享内容和类型
注意，数据体必须使用Json字符串，否则会报参数错误
```javascript
computed: {
  //放在计算属性下
  shareContent(){
    console.log(this.currentSteam)
    return {
      description:'steam游戏',
      data:JSON.stringify({
        type:'game',//标记类型，用于渲染判断
        game:{
          ...this.currentSteam,
          header:'https://cdn.cloudflare.steamstatic.com/steam/apps/'
            +this.currentSteam.appid+'/header.jpg',
          icon:getClientIcon(this.currentSteam.appid,this.currentSteam.clientIcon),
        }
      })
    }
  }
}
```
第三步.构建渲染结构：
```html
 <div v-if="isCustom.type==='game'">
  <!--      {{steam游戏格式}}-->
  <img style="border-radius: 5px;width: 250px" :src="isCustom.game?.header">
  <div class="mt-2 " style="margin-top: 10px">
    <div class="truncate" style="max-width: 160px;float: left;margin-right: 10px">
      <a-avatar :size="30" shape="square" :src="isCustom.game?.icon"></a-avatar> 
        <strong>{{isCustom.game?.chineseName}}</strong>
  </div>

  <div  style="float: right">
    <xt-button @click="goGameDetail(isCustom.game?.appid)" :w="70" :h="35" size="mini">
        <ShopOutlined></ShopOutlined> 查看</xt-button>
  <div style="clear: both">
  </div>
</div>
</div>
</div>
```
`vite/packages/table/TUIKit/TUIComponents/container/TUIChat/components/message-custom.vue`
参考steam游戏格式写法进行渲染
```javascript

//路由跳转
const goGameDetail=(id)=>{
      router.push({
          name:'GameDiscountDetail',
          params:{
            id:id
          }
        })
    }
```
注意：其他人需要能解析这个类型，得更新客户端

## ShareContent格式

`注意：建议尽量提交userInfo对象，以加速弹出速度。`
### 1. game #游戏格式

| 属性          | 说明   | 类型     | 默认值     |
|:------------|:-----|:-------|:--------|
| appid       | 游戏id | string | 必填，用于跳转 | 
| header      | 封面图  | string | 必填      | 
| chineseName | 中文名  | string | 必选      | 
| icon        | 图标   | string | 可选      | 