import { defineUserConfig } from "vuepress";
import theme from "./theme.js";
import {hopeTheme} from "vuepress-theme-hope";

export default defineUserConfig({
  base: "/ttdoc/",

  lang: "zh-CN",
  title: "天天工作台官方文档站",
  description: "提供天天工作台相关技术文档",

  theme

  // Enable it with pwa
  // shouldPrefetch: false,
});
