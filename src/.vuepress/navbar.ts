import { navbar } from "vuepress-theme-hope";

export default navbar([
    "/",
    "/common/",
    "/ttapp/",
    "/components/",
    "/business/",
    "/social/",
    "/tsbapi/",
    {
        text: "TSBApi文档",
        icon: "book",
        link: "https://a.apps.vip/docs",
    },
]);
