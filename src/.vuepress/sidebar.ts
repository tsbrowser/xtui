import { sidebar } from "vuepress-theme-hope";

export default sidebar({
    "/":  [
        {
            text: "开发必看",
            link: "",
        },
    ],

    "/social/": [
        {
            text: "总览",
            link: "overview.md",
        },
        {
            text: "通用",
            icon: "book",
            link: "",
            children: [
                {
                    text: "UserCard 用户卡片",
                    link: "usercard.md",
                },
            ],
        },
        {
            text: "IM相关",
            icon: "book",
            link: "",
            children: [
                {
                    text: "ShareToChat 分享到聊天",
                    link: "sharetochat.md",
                },
                {
                    text: "AddFriendButton 添加好友按钮",
                    link: "addfriend.md",
                },
                {
                    text: "SendMessageButton 发消息按钮",
                    link: "addfriend.md",
                },
            ],
        },
    ],

    "/tsbapi/": [
        {
            text: "高阶",
            children: [
                {
                    text: "db",
                    link: "db.md",
                },
            ],
        },
    ],
    "/components/": [
        {
            text: "总览",
            link: "overview.md",
        },
        {
            text: "通用",
            icon: "book",
            link: "",
            children: [
                {
                    text: "Tab",
                    link: "tab.md",
                },
                {
                    text: "Button 按钮",
                    link: "button.md",
                },
                {
                    text: "Icon 图标组件",
                    link: "icon.md",
                },
                {
                    text: "XtIcon 二次封装的图标组件",
                    link: "xticon.md",
                },
                {
                    text: "XtNewIcon 新图标库图标组件",
                    link: "newIcon.md",
                },
                {
                    text: "Emoji 表情组件",
                    link: "emoji.md",
                },
                {
                    text: "collapse 折叠面板",
                    link: "collapse.md",
                },
                {
                    text: "selectIcon 图标表情选择器",
                    link: "selectIcon.md",
                },
            ],
        },
    ],

    "/business/": [
        {
            text: "总览",
            icon: "book",
            link: "",
            children: [
                {
                    text: "View 全屏、弹窗、以淘汰",
                    link: "view.md",
                },
                {
                    text: "替换之前的View组件，新的弹窗",
                    link: "modal.md",
                },
                {
                    text: "Menu 右键菜单",
                    link: "menu.md",
                },
                {
                    text: "任务系统",
                    link: "task.md",
                },
                {
                    text: "LeftMenu 左侧菜单",
                    link: "leftMenu.md",
                },
            ],
        },
    ],
    "/ttapp/": [
        {
            text: "ttapp",
            link: "",
            children: [
                {
                    text: "manifest配置说明",
                    link: "manifest.md",
                },
                {
                    text: "主题使用",
                    link: "theme.md",
                },
            ],
        },
        {
            text: "开发教程",
            link: "",
            children: [
                {
                    text: "第一课.初始化项目",
                    link: "l1.md",
                },
                {
                    text: "第二课.调试",
                    link: "l2.md",
                },
                {
                    text: "第三课.主题色适配",
                    link: "l3.md",
                },
            ],
        },
    ],
});
// "slides":{
//     "/components/": [
//         {
//             text: '通用',
//         },
//         {
//             text: '导航'
//         },
//         {
//             text: '数据录入'
//         },
//         {
//             text: '数据展示'
//         },
//         {
//             text: '反馈'
//         },
//         {
//             text: '其他'
//         }
//     ]
// },
// ],
