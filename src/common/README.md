---
title: 开发必看
index: false
icon: laptop-code
category:
  - 使用指南
---

# 第一次开发可以阅读一遍

# 第一次开发可以阅读一遍

## 常用类名

xt-bg 主要背景色
xt-bg-2 次要背景色
xt-theme-bg 主题色
xt-theme-bg-2 次要主题色
xt-modal 弹窗背景色
xt-text 主要字体颜色
xt-text-2 次要字体颜色
xt-b 默认边框色 扩展 br 右 bl 左 bt 上 bb 下
xt-theme-b 主题边框色 扩展 br 右 bl 左 bt 上 bb 下
xt-font 主要字体
xt-num-font 主要数字字体

## 常用组件

xt-button 按钮
xt-new-icon 图标
xt-model 弹窗

## 全局环境

全局都挂载了`currentEnv` `isWeb` `isClient` `isMac` 四个变量 除了 import 方式都需要加 `$`

js/ts

```js
import currentEnv, { isWeb, isClient, isMac } from "./ui/hooks/env";
```

composition api

```vue
<template>
  <!-- 只在模板使用可以不注入 -->
  <p>{{ $isClient }}</p>
  <p>{{ $isWeb }}</p>
</template>

<script setup>
// 在脚本使用需要注入
import { inject } from "vue";
const $isWeb = inject("isWeb");
console.log("$isWeb :>> ", $isWeb);
</script>
```

options api

```vue
<template>
  <p>{{ $isWeb }}</p>
</template>

<script>
export default {
  mounted() {
    console.log("this.$isClient :>> ", this.$isClient);
  },
};
</script>
```

## 全局样式

用于统一项目整体样式风格

路径：browser\vite\public\css\styleSwitch
样式变量：browser\vite\public\css\styleSwitch\style.scss
类名变量：browser\vite\public\css\styleSwitch\class.scss
二次封装的通用类名变量：browser\vite\public\css\styleSwitch\components.scss

## 全局字体

路径：browser\vite\public\css\fonts
类名变量：browser\vite\public\css\fonts\class.scss

## 公共组件

用于统一项目整体样式风格

大部分 antd 组件的样式已在组件上进行了二次封装，优先使用写好的公共组件，文档不全找不到可以先去目录看看有没有相关组件

路径：browser\vite\packages\table\ui
基于 antd 组件封装存放在 libs
基于项目业务的组件封装的存放在 component

确定项目多处跨页面同时需要使用可以根据路径创建相关公共组件

## 公共方法

路径 1：browser\vite\packages\table\components\card\hooks
路径 2：browser\vite\packages\table\ui\hooks

## 卡片开发流程

卡片存放路径：browser\vite\packages\table\components\widgets
卡片开发规范：browser\vite\packages\table\components\card\Widget.vue
添加路径
挂载路径

## 在哪里了解如何编写此文档？

可以参考 hope 的相关文档

[hope 官方文档](https://theme-hope.vuejs.press/zh/guide/)
