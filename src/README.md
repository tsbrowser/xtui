---
home: true
icon: home
title: 首页
heroImage: /logo.png
bgImage: https://theme-hope-assets.vuejs.press/bg/6-light.svg
bgImageDark: https://theme-hope-assets.vuejs.press/bg/6-dark.svg
bgImageStyle:
  background-attachment: fixed
heroText: 天天工作台在线文档
tagline: 本站主要围绕天天工作台项目展开，包括一些开发方面的知识和相关SDK的说明
actions:
  - text: 💡 Gitee项目 
    link: https://gitee.com/tsbrowser/xiangtian-workbench
    type: primary

  - text: 官网
    link: https://www.apps.vip
    
  - text:  加入QQ群
    link: https://qm.qq.com/q/y8A46nqHMk


highlights:
  - header: 如何开发天天工作台第三方应用？
    image: /assets/image/box.svg
    bgImage: https://theme-hope-assets.vuejs.press/bg/3-light.svg
    bgImageDark: https://theme-hope-assets.vuejs.press/bg/3-dark.svg
    highlights:
      - title: 天天工作台具备第三方应用开发机制，您可以使用web前端技术开发自己的应用
      - title: 您的应用运行在天天工作台基座之上，故天然对副屏有良好的兼容性
      - title: 可查看此处了解应用机制
        link: /ttapp/


 



